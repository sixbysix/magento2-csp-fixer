<?php
declare(strict_types=1);

namespace SixBySix\CspFixer\Model\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use SixBySix\CspFixer\Api\PolicyTypeInterface;

class PolicyType implements PolicyTypeInterface
{
    const DEFAULT_SRC = 'default-src';
    const BASE_URI = 'base-uri';
    const CHILD_SRC = 'child-src';
    const CONNECT_SRC = 'connect-src';
    const FONT_SRC = 'font-src';
    const FORM_ACTION = 'form-action';
    const FRAME_ANCESTORS = 'frame-ancestors';
    const FRAME_SRC = 'frame-src';
    const IMG_SRC = 'img-src';
    const MANIFEST_SRC = 'manifest-src';
    const MEDIA_SRC = 'media-src';
    const OBJECT_SRC = 'object-src';
    const SCRIPT_SRC = 'script-src';
    const STYLE_SRC = 'style-src';

    /**
     * @return array<int, array<string, \Magento\Framework\Phrase|string>>
     */
    public function toOptionArray(): array
    {
        return [
            ['value' => self::DEFAULT_SRC, 'label' => __('Default (default-src)')],
            ['value' => self::BASE_URI, 'label' => __('Base URI (base-uri)')],
            ['value' => self::CHILD_SRC, 'label' => __('Child (child-src)')],
            ['value' => self::CONNECT_SRC, 'label' => __('Connect (connect-src)')],
            ['value' => self::FONT_SRC, 'label' => __('Font (font-src)')],
            ['value' => self::FORM_ACTION, 'label' => __('Form (form-action)')],
            ['value' => self::FRAME_ANCESTORS, 'label' => __('Frame Ancestors (frame-ancestors)')],
            ['value' => self::FRAME_SRC, 'label' => __('Frame (frame-src)')],
            ['value' => self::IMG_SRC, 'label' => __('Image (img-src)')],
            ['value' => self::MANIFEST_SRC, 'label' => __('Manifest (manifest-src)')],
            ['value' => self::MEDIA_SRC, 'label' => __('Media (media-src)')],
            ['value' => self::OBJECT_SRC, 'label' => __('Object (object-src)')],
            ['value' => self::SCRIPT_SRC, 'label' => __('Script (script-src)')],
            ['value' => self::STYLE_SRC, 'label' => __('Style (style-src)')],
        ];
    }
}