<?php
declare(strict_types=1);

namespace SixBySix\CspFixer\Model;

use SixBySix\CspFixer\Api\ConfigInterface;

/**
 * Class Config
 * @package SixBySix\CspFixer\Model
 */
class Config implements ConfigInterface
{
    public function __construct(
        readonly protected \Magento\Framework\App\Config $config
    ) {
    }

    /**
     * @inheritDoc
     */
    public function isFixerEnabled(): bool
    {
        return (bool) $this->getConfigValue(self::XML_PATH_FIXER_ENABLED);
    }

    /**
     * @inheritDoc
     */
    public function isFixerDebugMode(): bool
    {
        return (bool) $this->getConfigValue(self::XML_PATH_FIXER_DEBUG);
    }

    /**
     * @inheritDoc
     */
    public function isPolicyInjectionEnabled(): bool
    {
        return (bool) $this->getConfigValue(self::XML_PATH_POLICIES_ENABLED);
    }

    /**
     * @inheritDoc
     */
    public function getPolicies(): array
    {
        $policies = (string) $this->getConfigValue(self::XML_PATH_POLICIES);
        $policies = json_decode($policies, true);
        return is_array($policies) ? $policies : [];
    }


    protected function getConfigValue(string $path): mixed
    {
        return $this->config->getValue($path);
    }
}
