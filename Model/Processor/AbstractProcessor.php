<?php
declare(strict_types=1);

namespace SixBySix\CspFixer\Model\Processor;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Helper\SecureHtmlRenderer;
use SixBySix\CspFixer\Api\ConfigInterface;
use SixBySix\CspFixer\Api\LoggerInterface;
use SixBySix\CspFixer\Api\ProcessorInterface;

/**
 * Class AbstractProcessor
 * @package SixBySix\CspFixer\Model\Processor
 */
abstract class AbstractProcessor implements ProcessorInterface
{
    public function __construct(
        protected readonly SecureHtmlRenderer $secureHtmlRenderer,
        protected readonly LoggerInterface $logger,
        protected readonly ConfigInterface $config,
        protected readonly UrlInterface $url
    )
    {}

    /**
     * Replace the old HTML with the new HTML
     * @param string|false $oldHtml
     * @param string|false $newHtml
     * @param string $domHtml
     * @param array<string> $tags
     * @return string
     */
    protected function replaceHtml(
        string|false $oldHtml,
        string|false $newHtml,
        string $domHtml,
        array $tags
    ): string
    {
        if ($oldHtml === false || $newHtml === false) {
            return $domHtml;
        }

        if (sizeof($tags) > 0) {
            $newHtml = $newHtml . "\n" . implode("\n", $tags);
        }
        $this->logFix($oldHtml);
        return str_replace($oldHtml, $newHtml, $domHtml);
    }

    protected function logFix(string $htmlReplaced): void
    {
        if ($this->config->isFixerDebugMode()) {
            $url = $this->url->getCurrentUrl();
            $this->logger->debug("{$url}: {$htmlReplaced}");
        }
    }
}