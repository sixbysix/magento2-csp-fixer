<?php
declare(strict_types=1);

namespace SixBySix\CspFixer\Model\Processor;

class EventListenerProcessor extends AbstractProcessor
{
    public function process(\DOMDocument $dom, \DOMXPath $xpath, string $html): string
    {
        $replaceNodes = [];

        $attributes = $this->getMatchingAttributes($xpath);
        if ($attributes === false) {
            return $html;
        }

        foreach ($attributes as $attr) {

            // determine an identifier for event's parent element
            /** @var \DOMElement $element */
            $element = $attr->ownerElement;
            $elementId = $element->getAttribute('id');
            if (empty($elementId)) {
                // generate a random string if the element does not have an id
                $elementId = uniqid('element_');
            }

            if (!isset($replaceNodes[$elementId])) {
                $replaceNodes[$elementId] = [
                    'origHtml' => $element->ownerDocument->saveHTML($element),
                    'newHtml' => null,
                    'tags' => [],
                ];
            }

            $element->setAttribute('id', $elementId);

            $tag = $this->secureHtmlRenderer->renderEventListenerAsTag(
                $attr->name,
                $attr->value,
                "#{$elementId}"
            );

            $element->removeAttribute($attr->name);

            $replaceNodes[$elementId]['tags'][] = $tag;
            $replaceNodes[$elementId]['newHtml'] = $element->ownerDocument->saveHTML($element);
        }

        foreach ($replaceNodes as $elementId => $element) {
            $html = $this->replaceHtml($element['origHtml'], $element['newHtml'], $html, $element['tags']);
        }

        return $html;
    }

    /**
     * @param \DOMXPath $xpath
     * @return \DOMNodeList<\DOMNode>|false
     */
    protected function getMatchingAttributes(\DOMXPath $xpath): \DOMNodeList|false
    {
        return $xpath->query('//@*[substring(local-name(), 1, 2) = "on"]');
    }
}