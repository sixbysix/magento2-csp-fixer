<?php
declare(strict_types=1);

namespace SixBySix\CspFixer\Model\Processor;

/**
 * Class InlineScriptProcessor
 * @package SixBySix\CspFixer\Model\Processor
 *
 * This class is responsible for processing inline scripts and adding
 * the nonce attribute to them, preventing them from being blocked by
 * the CSP.
 */
class InlineScriptProcessor extends AbstractProcessor
{
    /**
     * Process the HTML and add the nonce attribute to the inline scripts
     */
    public function process(\DOMDocument $dom, \DOMXPath $xpath, string $html): string
    {
        $nodes = $this->getNodes($xpath);
        if ($nodes === false) {
            return $html;
        }

        foreach ($nodes as $node) {

            $attributes = [];
            if (!is_null($node->attributes)) {
                foreach ($node->attributes as $attribute) {
                    $attributes[$attribute->nodeName] = $attribute->nodeValue;
                }
            }

            if (isset($attributes['src'])) {
                continue;
            }

            if (isset($attributes['nonce'])) {
                continue;
            }

            if (isset($attributes['type']) && $attributes['type'] !== 'text/javascript') {
                continue;
            }

            $origNodeHtml = $node->ownerDocument->saveHTML($node);

            $tag = $this->secureHtmlRenderer->renderTag(
                'script',
                $attributes,
                $node->nodeValue,
                false
            );

            $html = $this->replaceHtml($origNodeHtml, $tag, $html, []);
        }

        return $html;
    }

    /**
     * @param \DOMXPath $xpath
     * @return \DOMNodeList<\DOMNode>|false
     */
    protected function getNodes(\DOMXPath $xpath): \DOMNodeList|false
    {
        return $xpath->query('//script');
    }
}