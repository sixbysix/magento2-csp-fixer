<?php
declare(strict_types=1);

namespace SixBySix\CspFixer\Plugin;

use SixBySix\CspFixer\Api\ConfigInterface;

class CspWhitelistXmlDataPlugin
{
    public function __construct(
        readonly protected ConfigInterface $config
    ) { }

    /**
     * @param \Magento\Csp\Model\Collector\CspWhitelistXml\Data $subject
     * @param array<string,string> $result
     * @return array<string,string>
     */
    public function afterGet(
        \Magento\Csp\Model\Collector\CspWhitelistXml\Data $subject,
        array $result
    ): array
    {
        if ($this->config->isPolicyInjectionEnabled()) {
            foreach ($this->config->getPolicies() as $policy) {
                if (isset($result[$policy['type']])) {
                    $result[$policy['type']]['hosts'][] = (string) $policy['value'];
                }
            }
        }

        return $result;
    }
}