<?php
declare(strict_types=1);

namespace SixBySix\CspFixer\Api;

interface LoggerInterface extends \Psr\Log\LoggerInterface
{

}