<?php
declare(strict_types=1);

namespace SixBySix\CspFixer\Api;

/**
 * Interface ProcessorInterface
 * @package SixBySix\CspFixer\Api
 */
interface ProcessorInterface
{
    /**
     * Identify corrections in the HTML and apply them
     * @param \DOMDocument $dom
     * @param \DOMXPath $xpath
     * @param string $html
     * @return string
     */
    public function process(\DOMDocument $dom, \DOMXPath $xpath, string $html): string;

}