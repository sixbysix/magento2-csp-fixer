<?php
declare(strict_types=1);

namespace SixBySix\CspFixer\Api;

/*
 * Interface ConfigInterface
 * @package SixBySix\CspFixer\Api
 */
interface ConfigInterface
{
    const XML_PATH_FIXER_ENABLED = 'csp/fixer/enable';
    const XML_PATH_FIXER_DEBUG = 'csp/fixer/debug';
    const XML_PATH_POLICIES_ENABLED = 'csp/policies/enable';
    const XML_PATH_POLICIES = 'csp/policies/policies';

    /**
     * Is fixer enabled in config?
     * @return bool
     */
    public function isFixerEnabled(): bool;

    /**
     * Is debug mode enabled in config?
     * @return bool
     */
    public function isFixerDebugMode(): bool;

    /**
     * Is policy injection enabled in config?
     * @return bool
     */
    public function isPolicyInjectionEnabled(): bool;

    /**
     * Get injection policies from config
     * @return array<array<string>>
     */
    public function getPolicies(): array;
}
