# SixBySix: Magento 2 CSP Fixer

https://gitlab.com/sixbysix/magento2-csp-fixer

---

This extension provides a number of tools to help you implement a Content Security Policy (CSP) on your Magento 2 store.

Currently Magento stores may operate their CSP policies in report-only mode and 
remain compliant with PCI DSS. However, this is not a long-term solution and from 
March 2025 all Magento stores will be required to have a fully operational CSP.

## Features
- **HTML Fixer**
  - Converts inline event listeners to tag-based event listeners.xs
  - Converts inline styles to tag-based styles.
  - Converts inline scripts to tag-based scripts.
  - Generates nonce attributes for inline scripts and styles.
- **CSP directive management** 
  - Manage your CSP directives from the Magento admin panel.

## Installation
1. Add the extension to your Magento 2 store using Composer:
    ```bash
    composer require sixbysix/magento2-csp-fixer
    ```
2. Enable the extension:
    ```bash
    bin/magento module:enable SixBySix_CspFixer
    bin/magento setup:upgrade
    ```

## Configuration

### Control mode (Report-Only or Strict-Mode)

1. Navigate to `Stores > Configuration > Security > Content Security Policy (CSP)`.
2. Open the `Mode` section.
3. Under each section you can enable/disable "Report-Only" mode.

![CSP Mode Configuration](docs/img/csp-mode-config.png)

### Enable fixer

1. Navigate to `Stores > Configuration > Security > Content Security Policy (CSP)`.
2. Open the `CSP Fixer` section.
3. Set `Enabled` to `Yes`.
4. If you enable the "Debug" option, the fixer will log the changes it makes to the page in MAGE_ROOT/var/log/sixbysix_cspfixer.log.

![CSP Fixer Configuration](docs/img/csp-fixer-config.png)

### Manage CSP Policies

1. Navigate to `Stores > Configuration > Security > Content Security Policy (CSP)`.
2. Open the `CSP Policies` section.
3. Enable or disable the CSP policy injection using the `Enabled` field.
4. Under the `Policies` field, you can add your CSP policies. These will be injected into
the CSP whitelist during the page load.

![CSP Policies Configuration](docs/img/csp-policies-config.png)

## Testing
Commits to this repository will trigger a GitLab CI pipeline that will run the following tests.

*Note: all tests are executed on a clean Magento 2.4.\* instance*

- **phpstan**
  - ```vendor/bin/phpstan analyse --no-progress --error-format=gitlab vendor/sixbysix/magento2-csp-fixer -c vendor/sixbysix/magento2-csp-fixer/phpstan.neon --level 8```
- **phpunit**
  -  ```XDEBUG_MODE=coverage vendor/bin/phpunit -c vendor/sixbysix/magento2-csp-fixer/phpunit.xml --coverage-html vendor/sixbysix/magento2-csp-fixer/coverage vendor/sixbysix/magento2-csp-fixer/```