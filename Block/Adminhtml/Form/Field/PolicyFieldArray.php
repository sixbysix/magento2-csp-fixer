<?php
declare(strict_types=1);

namespace SixBySix\CspFixer\Block\Adminhtml\Form\Field;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Framework\View\Element\BlockInterface;
use SixBySix\CspFixer\Block\Adminhtml\Form\Field\PolicyFieldArray\TypeSelect;

class PolicyFieldArray extends AbstractFieldArray
{
    protected function _prepareToRender()
    {
        $this->addColumn('type', [
            'label' => __('Type'),
            'class' => 'required-entry',
            'renderer' => $this->getTypeRenderer(),
        ]);

        $this->addColumn('value', [
            'label' => __('Value'),
            'class' => 'required-entry'
        ]);

        $this->_addAfter = false;
        $this->_addButtonLabel = (string) __('Add');
    }

    protected function _prepareArrayRow(\Magento\Framework\DataObject $row)
    {
        $options = [];
        $type = $row->getType();
        if ($type) {
            $options['option_' . $this->getValues($type)] = 'selected="selected"';
        }
        $row->setData('option_extra_attrs', $options);
    }

    protected function getTypeRenderer(): BlockInterface
    {
        return $this->getLayout()->createBlock(
            TypeSelect::class,
            '',
            ['data' => ['is_render_to_js_template' => true]]
        );
    }
}