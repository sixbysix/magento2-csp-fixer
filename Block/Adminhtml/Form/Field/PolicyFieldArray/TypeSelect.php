<?php
declare(strict_types=1);

namespace SixBySix\CspFixer\Block\Adminhtml\Form\Field\PolicyFieldArray;

use Magento\Framework\View\Element\Context;
use Magento\Framework\View\Element\Html\Select;
use SixBySix\CspFixer\Api\PolicyTypeInterface;

/**
 * Class TypeSelect
 * @package SixBySix\CspFixer\Block\Adminhtml\Form\Field\PolicyFieldArray
 */
class TypeSelect extends Select
{
    /**
     * TypeSelect constructor.
     * @param PolicyTypeInterface $policyType
     * @param Context $context
     * @param array<mixed> $data
     */
    public function __construct(
        protected PolicyTypeInterface $policyType,
        Context $context,
        array $data = []
    )
    {
        parent::__construct($context, $data);
    }

    public function setInputName(string $value): self
    {
        return $this->setName($value);
    }

    public function setInputId(string $value): self
    {
        return $this->setId($value);
    }

    public function _toHtml(): string
    {
        if (!$this->getOptions()) {
            $this->setOptions($this->policyType->toOptionArray());
        }
        return parent::_toHtml();
    }
}