<?php
declare(strict_types=1);

namespace SixBySix\CspFixer\Observer;

use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;
use SixBySix\CspFixer\Api\ConfigInterface;
use SixBySix\CspFixer\Api\ProcessorInterface;

class FixControllerResponseObserver implements ObserverInterface
{
    /**
     * @param ConfigInterface $config
     * @param array<ProcessorInterface> $processors
     * @param LoggerInterface $logger
     */
    public function __construct(
        readonly protected ConfigInterface $config,
        readonly protected array $processors,
        readonly protected LoggerInterface $logger
    )
    {}

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if (!$this->config->isFixerEnabled()) {
            return;
        }

        $response = $observer->getEvent()->getData('response');
        $responseContent = $response->getContent();

        if (!is_string($responseContent)) {
            return;
        }

        if (empty($responseContent)) {
            return;
        }

        $dom = new \DOMDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTML($responseContent);
        libxml_use_internal_errors(false);

        $xpath = new \DOMXPath($dom);

        foreach ($this->processors as $processor) {
            $responseContent = $processor->process($dom, $xpath, $responseContent);
        }

        $response->setContent($responseContent);
    }
}