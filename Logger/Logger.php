<?php
declare(strict_types=1);

namespace SixBySix\CspFixer\Logger;

use SixBySix\CspFixer\Api\LoggerInterface;

/**
 * Class Logger
 * @package SixBySix\CspFixer\Logger
 */
class Logger extends \Monolog\Logger implements LoggerInterface
{
}