<?php
declare(strict_types=1);

namespace SixBySix\CspFixer\Logger\Handler;

/**
 * Class Debug
 * @package SixBySix\CspFixer\Logger\Handler
 */
class Debug extends \Magento\Framework\Logger\Handler\Base
{
    protected $loggerType = \Psr\Log\LogLevel::DEBUG;
    protected $fileName = '/var/log/sixbysix_cspfixer.log';
}
