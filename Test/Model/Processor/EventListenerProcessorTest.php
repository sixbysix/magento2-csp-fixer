<?php
declare(strict_types=1);

namespace SixBySix\CspFixer\Test\Model\Processor;

use Magento\Framework\View\Helper\SecureHtmlRender\HtmlRenderer;
use Magento\Framework\View\Helper\SecureHtmlRenderer;
use SixBySix\CspFixer\Model\Processor\EventListenerProcessor;
use PHPUnit\Framework\TestCase;

/**
 * Class EventListenerProcessorTest
 * @package SixBySix\CspFixer\Test\Model\Processor
 */
class EventListenerProcessorTest extends AbstractProcessorTest
{
    public function testProcess(): void
    {
        // one event listener will be found and rewritten
        $this->mockSecureHtmlRenderer->expects($this->once())
            ->method('renderEventListenerAsTag')
            ->with('onclick', 'console.log(this)');

        // enable logging
        $this->mockLogger
            ->expects($this->once())
            ->method('debug');

        $html = '<html><head></head><body><script onclick="console.log(this)"></script></body></html>';
        $this->setDom($html);

        $processor = new EventListenerProcessor(
            secureHtmlRenderer: $this->mockSecureHtmlRenderer,
            logger: $this->mockLogger,
            config: $this->mockConfig,
            url: $this->mockUrl
        );

        $newHtml = $processor->process($this->dom, $this->xpath, $html);
        $this->assertNotEquals($html, $newHtml);
    }
}
