<?php
declare(strict_types=1);

namespace SixBySix\CspFixer\Test\Model\Processor;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Helper\SecureHtmlRenderer;
use SixBySix\CspFixer\Api\ConfigInterface;
use SixBySix\CspFixer\Api\LoggerInterface;

/**
 * Class AbstractProcessorTest
 * @package SixBySix\CspFixer\Test\Model\Processor
 */
abstract class AbstractProcessorTest extends \PHPUnit\Framework\TestCase
{
    protected ConfigInterface $mockConfig;
    protected SecureHtmlRenderer $mockSecureHtmlRenderer;
    protected LoggerInterface $mockLogger;
    protected UrlInterface $mockUrl;
    protected \DOMDocument $dom;
    protected \DOMXPath $xpath;

    protected function setUp(): void
    {
        parent::setUp();

        $this->mockConfig = $this->createMock(\SixBySix\CspFixer\Api\ConfigInterface::class);
        $this->mockSecureHtmlRenderer = $this->createMock(\Magento\Framework\View\Helper\SecureHtmlRenderer::class);
        $this->mockLogger = $this->createMock(LoggerInterface::class);
        $this->mockUrl = $this->createMock(\Magento\Framework\UrlInterface::class);

        $this->mockConfig->method('isFixerDebugMode')->willReturn(true);
    }

    protected function setDom(string $html): void
    {
        $this->dom = new \DOMDocument();
        $this->dom->loadHTML($html);

        $this->xpath = new \DOMXPath($this->dom);
    }
}