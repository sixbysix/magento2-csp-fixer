<?php
declare(strict_types=1);

namespace SixBySix\CspFixer\Test\Model\Processor;

use Magento\Framework\View\Helper\SecureHtmlRender\HtmlRenderer;
use Magento\Framework\View\Helper\SecureHtmlRenderer;
use SixBySix\CspFixer\Model\Processor\InlineScriptProcessor;
use PHPUnit\Framework\TestCase;

/**
 * Class InlineScriptProcessorTest
 * @package SixBySix\CspFixer\Test\Model\Processor
 */
class InlineScriptProcessorTest extends AbstractProcessorTest
{
    public function testProcess(): void
    {
        $this->mockSecureHtmlRenderer
            ->expects($this->once())
            ->method('renderTag')
            ->with('script', [], 'console.log(this);');

        $html = <<<html
<html>
<head></head>
<body>
<script>console.log(this);</script>
<script nonce="test">console.log('Dont change this')</script>
<script type="text/x-magento-init">console.log('Dont change this')</script>
<script src="/test.js"></script>
</body>
</html>
html;
        $this->setDom($html);

        $processor = new InlineScriptProcessor(
            secureHtmlRenderer: $this->mockSecureHtmlRenderer,
            logger: $this->mockLogger,
            config: $this->mockConfig,
            url: $this->mockUrl
        );

        $newHtml = $processor->process($this->dom, $this->xpath, $html);
        $this->assertNotEquals($html, $newHtml);
    }
}
