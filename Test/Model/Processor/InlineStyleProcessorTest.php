<?php
declare(strict_types=1);

namespace SixBySix\CspFixer\Test\Model\Processor;

use SixBySix\CspFixer\Model\Processor\InlineStyleProcessor;

/**
 * Class InlineStyleProcessorTest
 * @package SixBySix\CspFixer\Test\Model\Processor
 */
class InlineStyleProcessorTest extends AbstractProcessorTest
{
    public function testProcess(): void
    {
        $html = <<<html
<html>
    <body>
    <button style="color: red;">Button #1</button>
    </body>
</html>
html;
        $this->setDom($html);

        $processor = new InlineStyleProcessor(
            secureHtmlRenderer: $this->mockSecureHtmlRenderer,
            logger: $this->mockLogger,
            config: $this->mockConfig,
            url: $this->mockUrl
        );

        $newHtml = $processor->process($this->dom, $this->xpath, $html);
        $this->assertNotEquals($html, $newHtml);

    }
}