<?php
declare(strict_types=1);

namespace SixBySix\CspFixer\Test\Model\Source;

/**
 * Class PolicyTypeTest
 * @package SixBySix\CspFixer\Test\Model\Source
 */
class PolicyTypeTest extends \PHPUnit\Framework\TestCase
{
    public function testToOptionArray(): void
    {
        $policyType = new \SixBySix\CspFixer\Model\Source\PolicyType();
        $options = $policyType->toOptionArray();

        $this->assertIsArray($options);
        $this->assertNotEmpty($options);

        foreach ($options as $option) {
            $this->assertArrayHasKey('value', $option);
            $this->assertArrayHasKey('label', $option);
        }
    }
}