<?php
declare(strict_types=1);

namespace SixBySix\CspFixer\Test\Model;

use SixBySix\CspFixer\Model\Config;
use PHPUnit\Framework\TestCase;

/**
 * Class ConfigTest
 * @package SixBySix\CspFixer\Test\Model
 * @codeCoverageIgnore
 */
class ConfigTest extends TestCase
{
    private const XML_PATH_FIXER_ENABLED = 'csp/fixer/enable';
    private const XML_PATH_FIXER_DEBUG = 'csp/fixer/debug';
    private const XML_PATH_POLICIES_ENABLED = 'csp/policies/enable';
    private const XML_PATH_POLICIES = 'csp/policies/policies';

    private Config $config;
    private \Magento\Framework\App\Config $configMock;

    protected function setUp(): void
    {

        $this->configMock = $this->createMock(\Magento\Framework\App\Config::class);
        $this->config = new Config(config: $this->configMock);
    }

    public function testIsFixerEnabled(): void
    {
        $this->configMock->method('getValue')
            ->with(self::XML_PATH_FIXER_ENABLED)
            ->willReturnOnConsecutiveCalls(
                true,
                false,
            );

        $this->assertTrue($this->config->isFixerEnabled());
        $this->assertFalse($this->config->isFixerEnabled());
    }

    public function testIsFixerDebugMode(): void
    {
        $this->configMock->method('getValue')
            ->with(self::XML_PATH_FIXER_DEBUG)
            ->willReturnOnConsecutiveCalls(
                true,
                false,
            );

        $this->assertTrue($this->config->isFixerDebugMode());
        $this->assertFalse($this->config->isFixerDebugMode());
    }

    public function testIsPolicyInjectionEnabled(): void
    {
        $this->configMock->method('getValue')
            ->with(self::XML_PATH_POLICIES_ENABLED)
            ->willReturnOnConsecutiveCalls(
                true,
                false,
            );

        $this->assertTrue($this->config->isPolicyInjectionEnabled());
        $this->assertFalse($this->config->isPolicyInjectionEnabled());
    }

    public function testGetPolicies(): void
    {
        $this->configMock->method('getValue')
            ->with(self::XML_PATH_POLICIES)
            ->willReturn("[]");

        $this->assertIsArray($this->config->getPolicies());
    }
}
