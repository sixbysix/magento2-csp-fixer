<?php
declare(strict_types=1);

namespace SixBySix\CspFixer\Test\Plugin;

/**
 * Class CspWhitelistXmlDataPluginTest
 * @package SixBySix\CspFixer\Test\Plugin
 */
class CspWhitelistXmlDataPluginTest extends \PHPUnit\Framework\TestCase
{
    public function testAfterGet(): void
    {
        $config = $this->createMock(\SixBySix\CspFixer\Api\ConfigInterface::class);
        $config->method('getPolicies')->willReturn([
            ['type' => 'script-src', 'value' => 'example.com'],
            ['type' => 'script-src', 'value' => 'example.org'],
        ]);

        $subject = $this->createMock(\Magento\Csp\Model\Collector\CspWhitelistXml\Data::class);
        $subject->method('get')->willReturn([
            'script-src' => [
                'hosts' => ['example.com'],
            ],
        ]);

        $plugin = new \SixBySix\CspFixer\Plugin\CspWhitelistXmlDataPlugin($config);
        $result = $plugin->afterGet($subject, [
            'script-src' => [
                'hosts' => ['example.com'],
            ],
        ]);

        $this->assertEquals([
            'script-src' => [
                'hosts' => ['example.com', 'example.com', 'example.org'],
            ],
        ], $result);
    }
}