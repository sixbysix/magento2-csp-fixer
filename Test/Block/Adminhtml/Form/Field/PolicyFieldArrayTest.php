<?php
declare(strict_types=1);

namespace SixBySix\CspFixer\Test\Block\Adminhtml\Form\Field;

/**
 * Class PolicyFieldArrayTest
 * @package SixBySix\CspFixer\Test\Block\Adminhtml\Form\Field
 */
class PolicyFieldArrayTest extends \PHPUnit\Framework\TestCase
{
    public function testGetArrayRows(): void
    {
        $mockEventManager = $this->createMock(\Magento\Framework\Event\ManagerInterface::class);
        $mockEventManager->method('dispatch')->willReturnSelf();

        $mockScopeConfig = $this->createMock(\Magento\Framework\App\Config\ScopeConfigInterface::class);
        $mockScopeConfig->method('getValue')->willReturn('1');

        $mockLayout = $this->createMock(\Magento\Framework\View\LayoutInterface::class);
        $mockLayout->method('createBlock')->with(
            \SixBySix\CspFixer\Block\Adminhtml\Form\Field\PolicyFieldArray\TypeSelect::class,
            '',
            ['data' => ['is_render_to_js_template' => true]]
        )->willReturn($this->createMock(\Magento\Framework\View\Element\BlockInterface::class));

        $mockResolver = $this->createMock(\Magento\Framework\View\Element\Template\File\Resolver::class);

        $mockFilesystem = $this->createMock(\Magento\Framework\Filesystem::class);
        $mockFilesystem->method('getDirectoryRead')->willReturn($this->createMock(\Magento\Framework\Filesystem\Directory\ReadInterface::class));

        $mockValidator = $this->createMock(\Magento\Framework\View\Element\Template\File\Validator::class);
        $mockAppState = $this->createMock(\Magento\Framework\App\State::class);
        $mockLogger = $this->createMock(\Psr\Log\LoggerInterface::class);
        $mockEscaper = $this->createMock(\Magento\Framework\Escaper::class);

        $mockContext = $this->createMock(\Magento\Backend\Block\Template\Context::class);
        $mockContext->method('getEventManager')->willReturn($mockEventManager);
        $mockContext->method('getScopeConfig')->willReturn($mockScopeConfig);
        $mockContext->method('getLayout')->willReturn($mockLayout);
        $mockContext->method('getResolver')->willReturn($mockResolver);
        $mockContext->method('getFilesystem')->willReturn($mockFilesystem);
        $mockContext->method('getValidator')->willReturn($mockValidator);
        $mockContext->method('getAppState')->willReturn($mockAppState);
        $mockContext->method('getLogger')->willReturn($mockLogger);

        $mockFactoryElement = $this->createMock(\Magento\Framework\Data\Form\Element\Factory::class);
        $mockFactoryCollection = $this->createMock(\Magento\Framework\Data\Form\Element\CollectionFactory::class);

        $mockForm = $this->createMock(\Magento\Framework\Data\Form::class);

        $mockElement = $this->getMockBuilder(\Magento\Framework\Data\Form\Element\AbstractElement::class)
            ->setConstructorArgs(['escaper' => $mockEscaper, 'factoryElement' => $mockFactoryElement, 'factoryCollection' => $mockFactoryCollection])
            ->setMethods(['getValue'])
            ->getMock();

        $mockElement->setForm($mockForm);

        $mockElement->method('getValue')->willReturn([
            0 => [
                'type' => 'script-src',
                'value' => 'self'
            ],
            1 => [
                'type' => 'script-src',
                'value' => 'self'
            ]
        ]);

        $policyType = new \SixBySix\CspFixer\Block\Adminhtml\Form\Field\PolicyFieldArray($mockContext);
        $policyType->setData('area', 'adminhtml');
        $policyType->render($mockElement);

        $this->assertIsArray($policyType->getArrayRows());
    }
}