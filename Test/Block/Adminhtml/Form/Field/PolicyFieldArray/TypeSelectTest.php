<?php
declare(strict_types=1);

namespace SixBySix\CspFixer\Test\Block\Adminhtml\Form\Field\PolicyFieldArray;

/**
 * Class TypeSelectTest
 * @package SixBySix\CspFixer\Test\Block\Adminhtml\Form\Field\PolicyFieldArray
 */
class TypeSelectTest extends \PHPUnit\Framework\TestCase
{
    public function testGetValues(): void
    {
        $mockEventManager = $this->createMock(\Magento\Framework\Event\ManagerInterface::class);
        $mockScopeConfig = $this->createMock(\Magento\Framework\App\Config\ScopeConfigInterface::class);
        $mockEscaper = $this->createMock(\Magento\Framework\Escaper::class);

        $mockContext = $this->createMock(\Magento\Framework\View\Element\Context::class);
        $mockContext->expects($this->once())->method('getEventManager')->willReturn($mockEventManager);
        $mockContext->expects($this->once())->method('getScopeConfig')->willReturn($mockScopeConfig);
        $mockContext->expects($this->once())->method('getEscaper')->willReturn($mockEscaper);

        $typeSelect = new \SixBySix\CspFixer\Block\Adminhtml\Form\Field\PolicyFieldArray\TypeSelect(
            (new \SixBySix\CspFixer\Model\Source\PolicyType()),
            $mockContext
        );

        $typeSelect->setInputName('test_name');
        $typeSelect->setInputId('test_id');
        $this->assertIsString($typeSelect->toHtml());
    }
}