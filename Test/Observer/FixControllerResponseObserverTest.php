<?php
declare(strict_types=1);

namespace SixBySix\CspFixer\Test\Observer;

use PHPUnit\Framework\MockObject\MockClass;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use SixBySix\CspFixer\Api\ConfigInterface;
use SixBySix\CspFixer\Api\ProcessorInterface;
use SixBySix\CspFixer\Observer\FixControllerResponseObserver;
use PHPUnit\Framework\TestCase;

/**
 * Class FixControllerResponseObserverTest
 * @package SixBySix\CspFixer\Test\Observer
 */
class FixControllerResponseObserverTest extends TestCase
{
    private ConfigInterface|MockObject $mockConfig;
    private ProcessorInterface|MockObject $mockProcessor;
    private LoggerInterface|MockObject $mockLogger;
    private \Magento\Framework\App\Response\Http|MockObject $mockResponse;
    private \Magento\Framework\Event|MockObject $mockEvent;
    private \Magento\Framework\Event\Observer|MockObject $mockObserverEvent;
    private FixControllerResponseObserver $observer;

    protected function setUp(): void
    {
        parent::setUp();

        $this->mockConfig = $this->createMock(\SixBySix\CspFixer\Api\ConfigInterface::class);

        $this->mockProcessor = $this->createMock(\SixBySix\CspFixer\Model\Processor\InlineScriptProcessor::class);

        $this->mockLogger = $this->createMock(\Psr\Log\LoggerInterface::class);

        $this->mockResponse = $this->createMock(\Magento\Framework\App\Response\Http::class);
        $this->mockEvent = $this->createMock(\Magento\Framework\Event::class);
        $this->mockEvent->method('getData')->with('response')->willReturn($this->mockResponse);

        $this->mockObserverEvent = $this->createMock(\Magento\Framework\Event\Observer::class);
        $this->mockObserverEvent->method('getEvent')->willReturn($this->mockEvent);

        $this->observer = new FixControllerResponseObserver(
            config: $this->mockConfig,
            processors: [
                $this->mockProcessor,
            ],
            logger: $this->mockLogger
        );
    }

    public function testExecute(): void
    {
        $this->mockConfig->method('isFixerEnabled')->willReturn(true);
        $this->mockResponse->method('getContent')->willReturn('<html></html>');
        $this->mockProcessor
            ->expects($this->once())
            ->method('process')
            ->willReturn('<html></html>');
        $this->observer->execute($this->mockObserverEvent);
    }

    public function testFixerDisabled(): void
    {
        $this->mockConfig->method('isFixerEnabled')->willReturn(false);
        $this->mockResponse
            ->expects($this->never())
            ->method('getContent');
        $this->mockProcessor
            ->expects($this->never())
            ->method('process');
        $this->observer->execute($this->mockObserverEvent);
    }

    public function testNonStringContent(): void
    {
        $this->mockConfig->method('isFixerEnabled')->willReturn(true);
        $this->mockResponse->method('getContent')->willReturn(1);
        $this->mockProcessor
            ->expects($this->never())
            ->method('process');
        $this->observer->execute($this->mockObserverEvent);
    }

    public function testEmptyContent(): void
    {
        $this->mockConfig->method('isFixerEnabled')->willReturn(true);
        $this->mockResponse->method('getContent')->willReturn('');
        $this->mockProcessor
            ->expects($this->never())
            ->method('process');
        $this->observer->execute($this->mockObserverEvent);
    }
}